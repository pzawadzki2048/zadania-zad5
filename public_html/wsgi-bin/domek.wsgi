#!/usr/bin/python
# -*- coding=utf-8 -*-

from cgi import parse_qs



class BookDB():
    def titles(self):
        titles = [dict(id=id, title=database[id]['title']) for id in database.keys()]
        return titles

    def title_info(self, id):
        return database[id]

    def return_db(self):
        return database


database = {
    'id1': {'title': 'CherryPy Essentials: Rapid Python Web Application Development',
            'isbn': '978-1904811848',
            'publisher': 'Packt Publishing (March 31, 2007)',
            'author': 'Sylvain Hellegouarch',
            },
    'id2': {'title': 'Python for Software Design: How to Think Like a Computer Scientist',
            'isbn': '978-0521725965',
            'publisher': 'Cambridge University Press; 1 edition (March 16, 2009)',
            'author': 'Allen B. Downey',
            },
    'id3': {'title': 'Foundations of Python Network Programming',
            'isbn': '978-1430230038',
            'publisher': 'Apress; 2 edition (December 21, 2010)',
            'author': 'John Goerzen',
            },
    'id4': {'title': 'Python Cookbook, Second Edition',
            'isbn': '978-0-596-00797-3',
            'publisher': 'O''Reilly Media',
            'author': 'Alex Martelli, Anna Ravenscroft, David Ascher',
            },
    'id5': {'title': 'The Pragmatic Programmer: From Journeyman to Master',
            'isbn': '978-0201616224',
            'publisher': 'Addison-Wesley Professional (October 30, 1999)',
            'author': 'Andrew Hunt, David Thomas',
            },
    }


def application(environ, start_response):
    d = parse_qs(environ['QUERY_STRING'])
    body = '<html><head><meta charset="utf-8" /><title>Zad1: Informacje CGI</title></head><body>'
    response_body = body

    dbase = BookDB()
    tytuly = dbase.titles()
    # print tytuly
    get_id = d.get('id', [''])[0]
    if get_id == "":


        for elem in tytuly:
            response_body = response_body + "<a href='?id=" + elem['id'] + "'>" + elem['title'] + "</a><br/>"
            response_body = response_body + "</body></html>"

    else:
        position_info = dbase.title_info(get_id)

        book_html = """
            Title: %s.<br/>
            <br/>
            Author: %s.<br/>
            <br/>
            Publisher: %s.<br/>
            <br/>
            ISBN: %s.<br/>
            <br/>
        """

        book_info = book_html % (
            position_info['title'],
            position_info['author'],
            position_info['publisher'],
            position_info['isbn'],

        )

        response_body = response_body + book_info
        response_body = response_body + "</body></html>"


    status = '200 OK'

    response_headers = [('Content-Type', 'text/html'),
                        ('Content-Length', str(len(response_body)))]
    start_response(status, response_headers)

    return [response_body]





