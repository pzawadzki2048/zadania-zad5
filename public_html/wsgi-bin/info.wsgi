#!/usr/bin/python
# -*- coding=utf-8 -*-

import datetime


body = """
<html>
    <head>
        <meta charset="utf-8" />
        <title>Zad1: Informacje CGI</title>
    </head>
    <body>
        Nazwa serwera to %s.<br>
        <br>
        Adres serwera to %s:%s.<br>
        <br>
        Nazwa twojego komputera to %s.<br>
        <br>
        Przybywasz z %s:%s.<br>
        <br>
        Aktualnie wykonywany skrypt to <tt>%s</tt>.<br>
        <br>
        Żądanie przyszło o %s.<br>
        <br>
        Ostatnia twoja wizyta miała miejsce %s.<br>
    </body>
</html>
"""

def application(environ, start_response):

    remote_host = ""
    try:
        remote_host = environ.get('REMOTE_HOST', 'Unset')
    except:
        remote_host = "|unknown|"
    now = datetime.datetime.now()

    response_body = body % (
         environ.get('SERVER_NAME', 'Unset'), # nazwa serwera
         environ.get('SERVER_ADDR', 'Unset'), # IP serwera
         environ.get('SERVER_PORT', 'Unset'), # port serwera
         remote_host, # nazwa klienta
         environ.get('REMOTE_ADDR', 'Unset'), # IP klienta
         environ.get('REMOTE_PORT', 'Unset'), # port klienta
         environ.get('SCRIPT_NAME', 'Unset'), # nazwa skryptu
         now, # bieżący czas
         environ.get('SERVER_NAME', 'Unset'), # czas ostatniej wizyty
    )
    status = '200 OK'

    response_headers = [('Content-Type', 'text/html'),
                        ('Content-Length', str(len(response_body)))]
    start_response(status, response_headers)

    return [response_body]

class Upperware:
    def __init__(self, app):
        self.wrapped_app = app

    def __call__(self, environ, start_response):
        for data in self.wrapped_app(environ, start_response):
            yield data.upper()

if __name__ == '__main__':
    from wsgiref.simple_server import make_server
    srv = make_server('', 31016, application)
    srv.serve_forever()

    Upperware.__init__(srv, application)
    Upperware.__call__(srv, )